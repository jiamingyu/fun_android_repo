package com.example.drillcase.retrofit

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.drillcase.R
import com.example.drillcase.dao.entity.Contact
import com.example.drillcase.fragment.ContactListFragmentDirections
import com.example.drillcase.presentation.DemoViewModel

const val ITEM = 0
const val LOADING = 1


class ContactAdapter(val contactEntities: List<Contact>, val viewModel: DemoViewModel): RecyclerView.Adapter<ContactHolder>() {

    init {


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_layout, parent, false)
        Log.i("On create","... ...")
        return ContactHolder(view)

    }

    override fun getItemViewType(position: Int): Int {
        Log.i("getItemViewType", "$position, ${contactEntities.size}")
        val lastRow = position==contactEntities.size - 1
        when (lastRow) {
            true -> {
                return LOADING
            }
            else -> return ITEM
        }
    }

    override fun getItemCount(): Int {
        return contactEntities?.size?:0
    }

    override fun onBindViewHolder(holder: ContactHolder, position: Int) {
        val entity = contactEntities!!.get(position)
        entity?:return

        Log.i("on Binding", "$position")

        holder.itemView.setOnClickListener {

            Toast.makeText(it.context, "${holder.name.text}", Toast.LENGTH_SHORT).show()

            val contactId = contactEntities[position]?.dtoId
            //Safe arg way
            val action = ContactListFragmentDirections.contactEditAction(contactId)
            it.findNavController().navigate(action)
        }

        holder.name.text = entity.name
        holder.cell.text = entity.cellPhone

        if (holder.itemViewType == LOADING ) {
            Log.i("Bind trigger", "Ready to trigger reload from db")
                viewModel.reloadContacts()
        }

        Log.i("Done onBinding position", "$viewModel")
    }
}

//TODO: ?? data class
class ContactHolder(v: View): RecyclerView.ViewHolder(v) {
    var name: TextView
    var cell: TextView

    init {
        name = v.findViewById<TextView>(R.id.item_name)
        cell = v.findViewById<TextView>(R.id.item_cellphone)

    }
}

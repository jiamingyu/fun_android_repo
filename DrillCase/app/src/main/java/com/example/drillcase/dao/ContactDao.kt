package com.example.drillcase.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.drillcase.dao.entity.Contact

@Dao
interface ContactDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertContact(contact: Contact)

    @Query("select * from contact_tbl")
    fun getAll(): LiveData<List<Contact>>

    @Query("delete from contact_tbl")
    fun deleteAll()
}
package com.example.drillcase.retrofit

import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {

    companion object {
        var BASE_URL = "http://www.mocky.io/"

        val gson = GsonBuilder().setLenient().create()
        val INSTANCE =  Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build()

        fun service(): ContactApiInterface {
            return INSTANCE.create(ContactApiInterface::class.java)
        }
    }
}

package com.example.drillcase.di

import com.example.drillcase.fragment.ContactEditFragment
import com.example.drillcase.fragment.ContactListFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface  ContactFragmentProvider {
    @ContributesAndroidInjector(modules = [ContactFragmentModule::class])
    fun provideContactFragmentFactory(): ContactListFragment

    @ContributesAndroidInjector(modules = [ContactFragmentModule::class])
    fun provideContactEditFragmentFactory(): ContactEditFragment
}
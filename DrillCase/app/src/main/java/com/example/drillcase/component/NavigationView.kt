package com.example.drillcase.component

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.annotation.AttrRes
import com.example.drillcase.R

/**
 * TODO: This nav viewGroup not being called & left here for further decision
 */
class NavigationView: FrameLayout {
    private lateinit var buttonsLayout: LinearLayout

    constructor(context: Context): super(context) {
        init()
    }
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs) {
        init()
    }
    constructor(context: Context, attrs: AttributeSet?, @AttrRes styleAttr: Int): super(context, attrs, styleAttr)  {
        init()
    }


    fun init() {
        // Define layout
        buttonsLayout = LinearLayout(context)
        buttonsLayout.orientation = LinearLayout.HORIZONTAL
        buttonsLayout.gravity = Gravity.CENTER_HORIZONTAL

        // Restrict toolbat width to something vaguely sensible on tablets
        var width = resources.getDimensionPixelSize(R.dimen.navigation_bar_width)
        if (width == 0) {
            width = FrameLayout.LayoutParams.MATCH_PARENT
        }
        val params = FrameLayout.LayoutParams(width, FrameLayout.LayoutParams.MATCH_PARENT)
        params.gravity = Gravity.CENTER_HORIZONTAL
        addView(buttonsLayout, params)

        rebuild()

    }

    private fun rebuild() {
        buttonsLayout.removeAllViews()

    }


}
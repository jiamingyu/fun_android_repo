package com.example.drillcase.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.drillcase.dao.entity.Contact
import kotlinx.coroutines.launch
import java.util.concurrent.atomic.AtomicBoolean
import javax.inject.Inject
import javax.inject.Singleton


@Singleton class DemoViewModel @Inject constructor(private val contactRepository: ContactRepository) : ViewModel(){
    var contacts: LiveData<List<Contact>> = contactRepository.getAll()
    // Use this as flag to guard UI not update until
    var underUpdate = AtomicBoolean(false)

    init {
        // This clear cache each time launch app
        reloadContacts()
    }

    fun reloadContacts() {
        underUpdate = AtomicBoolean(true)
        viewModelScope.launch {
            contactRepository.retrieveContacts()
            underUpdate = AtomicBoolean(false)
        }
    }

    fun clearCached() {
        viewModelScope.launch {
            contactRepository.scrubLocalCache()
        }
    }

}
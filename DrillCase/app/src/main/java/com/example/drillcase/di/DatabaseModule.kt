package com.example.drillcase.di

import android.app.Application
import androidx.room.Room
import com.example.drillcase.dao.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DatabaseModule {

    @Singleton
    @Provides
    fun provideDb(app: Application): AppDatabase  =
        Room.databaseBuilder(app, AppDatabase::class.java, "DemoDB").build()
}
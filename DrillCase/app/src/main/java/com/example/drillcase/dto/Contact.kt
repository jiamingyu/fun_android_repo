package com.example.drillcase.dto

import com.google.gson.annotations.SerializedName

class Contact (
    @field:SerializedName("id")
    var id: String,
    @field:SerializedName("name")
    var name: String,
    @field:SerializedName("cell")
    var cellPhone: String
)
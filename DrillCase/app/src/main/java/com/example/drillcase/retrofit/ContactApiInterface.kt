package com.example.drillcase.retrofit

import com.example.drillcase.dto.Contact
import retrofit2.Response
import retrofit2.http.GET


interface ContactApiInterface {
    /**
     * Response body: v2/5d242c832f000059002416cf?mocky-delay=3000ms
    [
        {"id": 10, "name": "Jack", "cell": "4343234456"},
        {"id": 30, "name": "Mary", "cell": "8389887676"},
        {"id": 33, "name": "Eric", "cell": "9923378989"},
    ]

    http://www.mocky.io/v2/5dfd4fd13100000e00c96d84
    Response body:

    [
    {"id": 10, "name": "Jack", "cell": "4343234456"},
    {"id": 30, "name": "Mary", "cell": "8389887676"},
    {"id": 33, "name": "Eric", "cell": "9923378989"},
    {"id": 53, "name": "Frank", "cell": "992000989"},
    {"id": 78, "name": "George", "cell": "692234989"},
    {"id": 110, "name": "Aack", "cell": "4343234456"},
    {"id": 130, "name": "Aary", "cell": "8389887676"},
    {"id": 133, "name": "Aric", "cell": "9923378989"},
    {"id": 102, "name": "XJack", "cell": "4343234456"},
    {"id": 302, "name": "XMary", "cell": "8389887676"},
    {"id": 332, "name": "XEric", "cell": "9923378989"},
    {"id": 1102, "name": "XAack", "cell": "4343234456"},
    {"id": 1302, "name": "XAary", "cell": "8389887676"},
    {"id": 1332, "name": "XAric", "cell": "9923378989"},
    {"id": 10, "name": "Y Jack", "cell": "4343234456"},
    {"id": 30, "name": "Y Mary", "cell": "8389887676"},
    {"id": 33, "name": "Y Eric", "cell": "9923378989"},
    {"id": 110, "name": "Y Aack", "cell": "4343234456"},
    {"id": 130, "name": "Y Aary", "cell": "8389887676"},
    {"id": 133, "name": "Y Aric", "cell": "9923378989"},
    {"id": 102, "name": "Y XJack", "cell": "4343234456"},
    {"id": 302, "name": "Y XMary", "cell": "8389887676"},
    {"id": 332, "name": "Y XEric", "cell": "9923378989"},
    {"id": 1102, "name": "Y XAack", "cell": "4343234456"},
    {"id": 1302, "name": "Y XAary", "cell": "8389887676"},
    {"id": 1332, "name": "YXAric", "cell": "9923378989"},
    ]

     *
     */
    @GET("v2/5dfd4fd13100000e00c96d84?mocky-delay=3000ms")
    suspend fun getAllContactsResp(): Response<List<Contact>>
}

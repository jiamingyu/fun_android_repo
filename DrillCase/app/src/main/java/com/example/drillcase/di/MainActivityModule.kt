package com.example.drillcase.di

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import com.example.drillcase.presentation.DemoViewModel
import com.example.drillcase.presentation.MainActivity
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface MainActivityModule {

    @Binds
    fun providesMainActivity(mainActivity: MainActivity): AppCompatActivity

    @Binds
    @IntoMap
    @ViewModelKey(DemoViewModel::class)
    fun bindDemoViewModel(demoViewModel: DemoViewModel): ViewModel

}
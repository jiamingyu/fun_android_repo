package com.example.drillcase.dao.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "contact_tbl")
data class Contact (
    @PrimaryKey(autoGenerate = true)
    val id: Int? = null,
    var dtoId: String,
    var name: String,
    var cellPhone: String
)
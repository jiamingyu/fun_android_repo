package com.example.drillcase.dao

import androidx.room.*
import com.example.drillcase.dao.entity.Contact

@Database(entities = [Contact::class], version = 1) //TODO: DB version should from gradle.properties
@TypeConverters(DateTyperConverter::class) //TODO: ready for new timestamp fields
abstract class AppDatabase: RoomDatabase(){
    abstract fun contactDao(): ContactDao

}
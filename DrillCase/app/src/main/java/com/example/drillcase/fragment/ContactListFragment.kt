package com.example.drillcase.fragment

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.graphics.drawable.DrawableCompat
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.drillcase.R
import com.example.drillcase.databinding.ContactListBinding
import com.example.drillcase.di.ViewModelFactory
import com.example.drillcase.presentation.DemoViewModel
import com.example.drillcase.retrofit.ContactAdapter
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ContactListFragment: DaggerFragment() {
    private lateinit var contactListBinding: ContactListBinding

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    var toolbar: Toolbar? = null

    val demoViewModel by lazy {
        this.activity?.let {
            ViewModelProviders.of(it, viewModelFactory).get(DemoViewModel::class.java)
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        setHasOptionsMenu(true)
        contactListBinding = ContactListBinding.inflate(inflater, container, false)
        val recyclerView = contactListBinding.recyclerId
        recyclerView.layoutManager = LinearLayoutManager(context)
        recyclerView.setHasFixedSize(true)
        demoViewModel?.contacts?.observe(viewLifecycleOwner, Observer{contacts ->
            if (!demoViewModel!!.underUpdate.get()) {
                recyclerView.adapter = ContactAdapter(contacts, demoViewModel!!)
            }
        })

        toolbar = contactListBinding.toolbar as Toolbar
        toolbar?.menu?.let{
            it.clear()
            onCreateOptionsMenu(it, activity?.menuInflater!!)
        }
        toolbar?.setOnMenuItemClickListener {
            when (it.itemId) {
                R.id.reloadContact -> {
                    demoViewModel?.reloadContacts()
                }
                R.id.cleanContact -> {
                    demoViewModel?.clearCached()
                }
                else -> Log.i("", "aha")
            }
            true
        }


        //KEEP, since it working for nav
        val x = DrawableCompat.wrap(ContextCompat.getDrawable(activity!!, R.drawable.ic_menu_chunky_24dp)!!.mutate())
        toolbar?.navigationIcon = x
        return contactListBinding.root
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
//        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.contact_list_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        when(item.itemId) {
            else -> Log.i("", "")
        }

        return super.onOptionsItemSelected(item)
    }

    companion object {
        val INSTANCE = ContactListFragment()
    }
}
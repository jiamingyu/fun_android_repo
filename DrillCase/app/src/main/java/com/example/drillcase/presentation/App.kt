package com.example.drillcase.presentation

import com.example.drillcase.di.DaggerContactAppComponent
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

open class App : DaggerApplication() {

    override fun onCreate() {
        super.onCreate()
        AndroidThreeTen.init(this)
    }

    /**
     * This is called by DaggerApplication.injectIfNecessary()
     *
     * Implementations should return an AndroidInjector for the concrete DaggerApplication.
     */
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerContactAppComponent.builder().application(this).build()
    }
}
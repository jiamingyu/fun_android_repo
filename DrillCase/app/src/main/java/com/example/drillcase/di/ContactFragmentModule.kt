package com.example.drillcase.di

import com.example.drillcase.fragment.ContactEditFragment
import com.example.drillcase.fragment.ContactListFragment
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
class ContactFragmentModule {
    @Singleton
    @Provides
    fun providesContactListfragment(): ContactListFragment {
        return ContactListFragment.INSTANCE
    }

    @Singleton
    @Provides
    fun providesContactEditFragment(): ContactEditFragment {
        return ContactEditFragment.INSTANCE
    }

}
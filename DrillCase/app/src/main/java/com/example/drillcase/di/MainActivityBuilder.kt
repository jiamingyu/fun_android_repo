package com.example.drillcase.di

import com.example.drillcase.presentation.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
interface MainActivityBuilder {
    /**
     *
     * @ContributesAndroidInjector generates an AndroidInjector for the return type of this method.
     * The injector is implemented with a Subcomponent and will be a child of the Module's component.
     * So @SubComponent should not be used any more
     */
    @ContributesAndroidInjector(modules = [MainActivityModule::class, ContactFragmentProvider::class])
    fun provideMainActivity(): MainActivity

}
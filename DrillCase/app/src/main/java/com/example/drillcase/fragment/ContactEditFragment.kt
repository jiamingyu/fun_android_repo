package com.example.drillcase.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.navArgs
import com.example.drillcase.databinding.NavContactEditBinding
import com.example.drillcase.di.ViewModelFactory
import com.example.drillcase.presentation.ContactRepository
import com.example.drillcase.presentation.DemoViewModel
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class ContactEditFragment: DaggerFragment() {
    private lateinit var contacteditBinding: NavContactEditBinding

    @Inject
    lateinit var contactRepository: ContactRepository

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    val demoViewModel: DemoViewModel? by lazy {
        this.activity?.run {
            ViewModelProviders.of(this, viewModelFactory)[DemoViewModel::class.java]
        }
    }


    // Must run on jdk 1.8 above
    val args: ContactEditFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        contacteditBinding = NavContactEditBinding.inflate(inflater, container, false)

    // Note: None safe-args way
//        val cid = arguments?.getString("contact_id")

        val cid = args.contactId // safe args way

        activity?.run {
            ViewModelProviders.of(this, viewModelFactory)[DemoViewModel::class.java]
        }

        demoViewModel?.contacts?.observe(viewLifecycleOwner, Observer{contacts ->
            if (!demoViewModel!!.underUpdate?.get()) {
                val cname = contacts.filter{
                    it.dtoId.equals(cid)
                }[0]?.name


                contacteditBinding.editContactName.setText(cname)

            }
        })
        return contacteditBinding.root
    }

    companion object {
        val INSTANCE = ContactEditFragment()
    }
}
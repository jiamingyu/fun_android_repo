package com.example.drillcase.di

import android.app.Application
import android.content.Context
import com.example.drillcase.dao.AppDatabase
import com.example.drillcase.presentation.ContactRepository
import com.example.drillcase.presentation.ContactRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
object AppModule {
    @Singleton
    @Provides
    @JvmStatic
    fun provideContext(application: Application): Context = application

    @Singleton
    @Provides
    @JvmStatic
    fun provideContactRepository(appDatabase: AppDatabase): ContactRepository
            = ContactRepositoryImpl(appDatabase )

}
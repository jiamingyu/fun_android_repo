package com.example.drillcase.presentation

import androidx.lifecycle.LiveData
import com.example.drillcase.dao.AppDatabase
import com.example.drillcase.retrofit.ApiClient
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


class ContactRepositoryImpl @Inject constructor(
    private val appDatabase: AppDatabase
) : ContactRepository {
    override suspend fun scrubLocalCache() = withContext(Dispatchers.IO) {
            appDatabase.contactDao().deleteAll()
    }

    override fun getAll(): LiveData<List<com.example.drillcase.dao.entity.Contact>> =
        appDatabase.contactDao().getAll()

    override suspend fun retrieveContacts() {
        val response = ApiClient.service().getAllContactsResp()
        if (response.isSuccessful) {

            withContext(Dispatchers.Default) {
                val contacts = response.body()

                appDatabase.contactDao().deleteAll()
                for (c in contacts!!) {
                    c?.apply {
                        val ce = com.example.drillcase.dao.entity.Contact(
                            dtoId = c.id,
                            name = c.name,
                            cellPhone = c.cellPhone
                        )
                        appDatabase.contactDao().insertContact(ce)
                    }
                }
            }

        }
    }

}

interface ContactRepository {
    suspend fun scrubLocalCache()
    suspend fun retrieveContacts()
    fun getAll(): LiveData<List<com.example.drillcase.dao.entity.Contact>>

}

package com.example.drillcase.retrofit

import okhttp3.OkHttpClient

object OkHttpProvider {
    val instance = OkHttpClient.Builder().build()
}
package com.example.drillcase.di

import android.app.Application
import com.example.drillcase.presentation.App
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    AppModule::class,
    ViewModelModule::class,
    DatabaseModule::class,
    MainActivityBuilder::class
])
interface ContactAppComponent : AndroidInjector<App> {

    @Component.Builder
    interface Builder {
        /**
         * add whatever method we want to add to builder.
         * In my case I wanted to add Application to my AppComponent.
         */
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ContactAppComponent
    }

    /**
     * This is called by DaggerApplication.injectIfNecessary():
     * AndroidInjector<DaggerApplication>.inject(this) // this therefore must be DaggerApplication.
     *
     * Through the call above,  all dependencies provided by AndroidInjector graph is accessible for 'this' instance
     *
     *
     * Injection cannot be performed in {@link
     * Application#onCreate()} since {@link android.content.ContentProvider}s' {@link
     * android.content.ContentProvider#onCreate() onCreate()} method will be called first and might
     * need injected members on the application. Injection is not performed in the constructor, as
     * that may result in members-injection methods being called before the constructor has completed,
     * allowing for a partially-constructed instance to escape.
     *
     *
     */
    override fun inject(app: App)
}
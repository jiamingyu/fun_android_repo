package com.example.drillcase.fragment

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.drillcase.R
import kotlinx.android.synthetic.main.reach_contact.view.*

class CallFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.reach_contact, container, false)

        view.callButton.setOnClickListener {

            val hasPermission = ContextCompat.checkSelfPermission(
                context!!,
                android.Manifest.permission.CALL_PHONE
            ) == PackageManager.PERMISSION_GRANTED

            val s = arrayOf<String>(android.Manifest.permission.CALL_PHONE.toString())

            if (!hasPermission) {
                requestPermissions(s, 1);

            }


            if (hasPermission) {
                val dialIntent = Intent(Intent.ACTION_CALL)
                val numberString = view.phoneNumber.text.toString()
                dialIntent.setData(Uri.parse("tel:" + numberString))
                startActivity(dialIntent)
            }

        }
        return view
    }
}
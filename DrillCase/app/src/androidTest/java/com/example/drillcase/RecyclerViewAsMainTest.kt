package com.example.drillcase

import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.drillcase.presentation.MainActivity
import com.example.drillcase.retrofit.ApiClient
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.util.concurrent.TimeUnit


/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class RecyclerViewAsMainTest {

    @get:Rule var idlingResourceRule = OkHttpIdlingResourceRule()

    @Before
    fun stubAllExternalIntents() {
        mockWebServer.start(8080)
        ApiClient.BASE_URL = mockWebServer.url("/").toString()
        ActivityScenario.launch(MainActivity::class.java)
    }

    @Test
    fun idleEnabledForCnotactsList() {
        val response = MockResponse().setBody(responseBody).setBodyDelay(1, TimeUnit.SECONDS)
        mockWebServer.enqueue(response)

        onView(withId(R.id.phoneNumber))

    }

    companion object {
        val PHONE_NUMBER_TEST_SET = "4148982222"
        val mockWebServer = MockWebServer()
        val responseBody = "[{\"id\": 10, \"name\": \"Jack\", \"cell\": \"4343234456\"},{\"id\": 30, \"name\": \"Mary\", \"cell\": \"8389887676\"},]"
    }
}

package com.example.drillcase

import android.app.Activity
import android.app.Instrumentation
import android.content.Intent
import android.net.Uri
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.*
import androidx.test.espresso.core.internal.deps.guava.collect.Iterables
import androidx.test.espresso.intent.Intents
import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.ext.truth.content.IntentSubject.assertThat
import androidx.test.rule.GrantPermissionRule
import org.hamcrest.Matchers
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

import androidx.test.espresso.intent.Intents.intended
import androidx.test.espresso.intent.matcher.IntentMatchers.*
import com.example.drillcase.presentation.MainActivity
import org.hamcrest.core.AllOf.allOf


/**
 *
 *
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@Deprecated("Should not run anymore.")
@RunWith(AndroidJUnit4::class)
class ExampleInstrumentedTest {
//    @get:Rule var activityTestRule = activityScenarioRule<MainActivity>()

    @get:Rule val activityTestRulePermission = GrantPermissionRule.grant("android.permission.CALL_PHONE");
    @get:Rule
    val activityTestRuleIntent = IntentsTestRule<MainActivity>(MainActivity::class.java)


    @Before
    fun stubAllExternalIntents() {
        Intents.intending(Matchers.not(isInternal())).respondWith(Instrumentation.ActivityResult(Activity.RESULT_OK, null))
    }



    @Test
    fun experiment() {
        onView(withId(R.id.phoneNumber)).perform(clearText(), typeText(PHONE_NUMBER_TEST_SET), closeSoftKeyboard())
        onView(withId(R.id.callButton)).perform(click())

        val receivedIntent = Iterables.getOnlyElement(Intents.getIntents())
        assertThat(receivedIntent).hasAction(Intent.ACTION_CALL)
        assertThat(receivedIntent).hasData(Uri.parse("tel:" + PHONE_NUMBER_TEST_SET))

        // Below is equivalent to above
        intended(allOf(
            hasAction(Intent.ACTION_CALL),
            hasData(Uri.parse("tel:" + PHONE_NUMBER_TEST_SET))
        )
        )

    }

    companion object {
        val PHONE_NUMBER_TEST_SET = "4148982222"
    }
}

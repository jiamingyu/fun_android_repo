
##
Ref: https://github.com/HenryChigbo/Retrofit2/

## Mock API response:
http://www.mocky.io/v2/5d242c832f000059002416cf

?mocky-delay=3000ms

Response delay
Add ?mocky-delay=100ms to delay responding (format / max: 60s).



#Technical issues and resolutions

## [UNSOLVED] https access decline even internet permission granted (device working, but emulator not working)
a. manifes xml config, add: android:networkSecurityConfig="@xml/network_security_config", to application
b. In res/xml/network_security_config.xml, ??? pending research: what to add as domain values

## xmlns:android="http://schemas.android.com/apk/res/android", needed in layout elements defining

## androidx recyclerView needs library update:
https://developer.android.com/jetpack/androidx/migrate,
use: androidx.recyclerview:recyclerview:1.0.0

## RecyclerView only show 1st item data:
In the item_layout.xml, where defining item template, root layout_height must be wrap_content, INSTEAD OF match content

## Retrofit test integrate with okhttp-idling-resource:
https://medium.com/insiden26/okhttp-idling-resource-for-espresso-462ef2417049
https://github.com/chiragkunder/espresso-okhttp-idling-resource

### A. network_securtiy_config, for test purpose adding "mocky.io" for mockito server setting. detail on commented part need more research
### B. OkHttpIdlingResourceRule, is copied from sample to demo how to use Jake W. 's okhttp3-idling-resource. Further research: https://github.com/JakeWharton/okhttp-idling-resource

## Annotation processing with kotlin better use kapt complier plugin, instead of using 'annotationProcessor'
https://kotlinlang.org/docs/reference/kapt.html

## Room
Code sample: https://github.com/googlecodelabs/android-room-with-a-view

## context vs context.applicationContext

*** draft plan
gradle
database, dao
data entity, type converter
?? db version: for now in database definitation annotation

# ModelView (In progress)
https://google-developer-training.github.io/android-developer-advanced-course-practicals/unit-6-working-with-architecture-components/lesson-14-room,-livedata,-viewmodel/14-1-a-room-livedata-viewmodel/14-1-a-room-livedata-viewmodel.html#task4intro



# Read Ref
https://www.raywenderlich.com/1560485-android-recyclerview-tutorial-with-kotlin#toc-anchor-009

https://codelabs.developers.google.com/codelabs/android-room-with-a-view/#0

https://medium.com/mindorks/room-kotlin-android-architecture-components-71cad5a1bb35

https://developer.android.com/topic/libraries/architecture/adding-components


https://developer.android.com/jetpack/androidx/releases/room (for further coroutin integration)

https://medium.com/mindorks/using-room-database-with-livedata-android-jetpack-cbf89b677b47

https://google-developer-training.github.io/android-developer-advanced-course-practicals/unit-6-working-with-architecture-components/lesson-14-room,-livedata,-viewmodel/14-1-a-room-livedata-viewmodel/14-1-a-room-livedata-viewmodel.html
https://codelabs.developers.google.com/advanced-android-training/

https://medium.com/androiddevelopers/understanding-migrations-with-room-f01e04b07929

https://medium.com/easyread/android-architecture-components-livedata-pt-1-133cad38e67e

https://github.com/googlesamples/android-architecture-components

https://medium.com/@elye.project/android-architecture-components-for-dummies-in-kotlin-50-lines-of-code-29b29d3a381




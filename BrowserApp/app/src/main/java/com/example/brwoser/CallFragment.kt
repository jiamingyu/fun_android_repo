package com.example.brwoser

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.brwoser.databinding.DummySubmitBinding
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class CallFragment : Fragment() {
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val submitViewBinding = DataBindingUtil.inflate<DummySubmitBinding>( inflater, R.layout.dummy_submit, container, false)

        submitViewBinding.callButton.setOnClickListener{
            // hide keyboard
            view?.run { hideKeyboard(context, applicationWindowToken) }



            val submitValue = submitViewBinding.phoneNumber
            sendRequest(submitValue.text.toString())
        }

        submitViewBinding.phoneNumber.setOnFocusChangeListener { view, b ->
            // move to keyboard util
            view?.requestFocus()
            val imm = view?.context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
            imm?.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)

        }

        return submitViewBinding.root
    }

    private fun hideKeyboard(context: Context?, iBinder: IBinder?) {
        (context as? Activity)?.run {
            val view = findViewById<View>(R.id.focus_sink)
            view?.requestFocus()
        }

        val mgr = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager
        iBinder?.run { mgr?.hideSoftInputFromWindow(this, 0) }
    }

    fun sendRequest(url: String) {
        val t = object: Thread() {
            override fun run() {
//                val url = URL("https://www.google.com/")
                val url = if (Patterns.WEB_URL.matcher(url).matches()) {
                    URL(url)
                } else {
                    URL("https://www.google.com/")
                }

                val httpsConn = url.openConnection() as HttpURLConnection

                val readTextBuf = StringBuffer()
                try {
                    // Set connection timeout and read timeout value.
                    httpsConn.connectTimeout = 10000
                    httpsConn.readTimeout = 10000

                    val inputStream = httpsConn.getInputStream()

                    // Create input stream reader based on url connection input stream.
                    val isReader = InputStreamReader(inputStream)

                    // Create buffered reader.
                    val bufReader = BufferedReader(isReader)

                    // Read line of text from server response.
                    var line = bufReader.readLine()

                    // Loop while return line is not null.
                    while (line != null) {
                        // Append the text to string buffer.
                        readTextBuf.append(line)

                        // Continue to read text line.
                        line = bufReader.readLine()
                    }

                    val showFragment = ShowFragment()
                    Handler(Looper.getMainLooper()).post{
                        activity!!.supportFragmentManager.beginTransaction().replace(R.id.contactRoot, showFragment).addToBackStack("submitFragment").commit()

                    }

                } finally {
                    httpsConn.disconnect()
                }
            }
        }
        t.start()
    }
}
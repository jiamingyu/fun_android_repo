package com.example.brwoser

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.example.brwoser.databinding.ShowFragmentBinding

class ShowFragment : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val showViewBinding = DataBindingUtil.inflate<ShowFragmentBinding>(
            inflater,
            R.layout.show_fragment,
            container,
            false
        )
        val htmlData = "<html><body>Hello World</body></html>"
        showViewBinding.webView.loadData(htmlData, "text/html", "UTF-8")

        return showViewBinding.root
    }
}